#include "cdtextfilemanager.h"

#include <QCoreApplication>
#include <QFile>
#include <QTextStream>

#include "cddefaults.h"

CDTextfileManager::CDTextfileManager(QObject *parent) : QObject(parent){}

void CDTextfileManager::saveFile(QString getFileName, QString getFileText) {
    QString patientName = QCoreApplication::arguments().at(1);
    QString workingPath = CDDefaults::getDefaultPatDir() + patientName;

    QFile file(workingPath + "/" + getFileName);
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream stream(&file);
    stream<<getFileText;
    file.close();
}

QString CDTextfileManager::readFile(QString getFileName) {
    QString returnMe="";
    QString patientName = QCoreApplication::arguments().at(1);
    QString workingPath = CDDefaults::getDefaultPatDir() + patientName;

    QFile file(workingPath + "/" + getFileName);
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    QTextStream stream(&file);
    returnMe = stream.readAll();
    file.close();

    return returnMe;
}
