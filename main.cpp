#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickImageProvider>

#include "cdtextfilemanager.h"
#include "cdgitmanager.h"
#include "cdimagemanager.h"
#include "cdinireader.h"
#include "cdtoollauncher.h"

int main(int argc, char *argv[])
{
    qputenv("QT_IM_MODULE", QByteArray("qtvirtualkeyboard"));

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    qmlRegisterType<CDINIReader>("dental.clear", 1, 0, "INIReader");
    qmlRegisterType<CDTextfileManager>("dental.clear", 1, 0, "TextFileManager");
    qmlRegisterType<CDGitManager>("dental.clear", 1, 0, "GitManager");
    qmlRegisterType<CDToolLauncher>("dental.clear", 1, 0, "ToolLauncher");

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("patientFileName",argv[1]);
    engine.addImageProvider("cdimage",new CDImageManager());
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
