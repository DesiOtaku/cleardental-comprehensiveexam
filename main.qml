import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.3
import dental.clear 1.0

ApplicationWindow {
    id: rootWin
    visible: true
    width: 1024
    height: 768
    title: qsTr("Comprehensive exam")


    Row {
        id: ccRow
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.leftMargin: 16
        anchors.topMargin: 16

        Label {
            text:"Chief Complaint:"
            font.pointSize: 16
            verticalAlignment: Text.AlignVCenter
        }

        RadioButton {
            id: noCCButton
            checked: true
            text: "None"
        }

        RadioButton {
            id: isACCButton
            text: "Other"
        }

        TextField {
            id: patCC
            opacity: isACCButton.checked? 1:0
            Behavior on opacity {
                PropertyAnimation {
                    duration: 300
                }
            }
        }
    }

    PatientInfoPanel {
        id: patInfoPanel
        anchors.left: ccRow.right
        anchors.right: parent.right
        anchors.top: parent.top
        //anchors.bottom: examButtons.bottom
        height: examButtons.height + ccRow.height + 16
        anchors.margins: 16
    }

    Column {
        id: examButtons
        anchors.top: ccRow.bottom
        anchors.left: parent.left
        anchors.right:ccRow.right
        anchors.margins: 16
        spacing: 16

        ToolLauncher {
            id: launcher
        }

        Button {
            text: "Medical Review"
            anchors.horizontalCenter: parent.horizontalCenter
        }

        Button {
            text: "Bitewings <strong>(due)</strong>"
            anchors.horizontalCenter: parent.horizontalCenter
        }

        Button {
            text: "Extraoral exam"
            anchors.horizontalCenter: parent.horizontalCenter
        }
        Button {
            text: "TMJ"
            anchors.horizontalCenter: parent.horizontalCenter
        }
        Button {
            text: "Intraoral / Soft tissue exam"
            anchors.horizontalCenter: parent.horizontalCenter
        }

        Button {
            text: "Hard tissue charting"
            anchors.horizontalCenter: parent.horizontalCenter
            onClicked: launcher.launchTool(ToolLauncher.HardTissue);
        }
    }

    TreatmentPlanPanel {
        anchors.top: examButtons.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 16
    }

    InputPanel {
        id: inputPanel
        z: 99
        x: 0
        y: rootWin.height
        width: rootWin.width

        states: State {
            name: "visible"
            when: inputPanel.active
            PropertyChanges {
                target: inputPanel
                y: rootWin.height - inputPanel.height
            }
        }
        transitions: Transition {
            from: ""
            to: "visible"
            reversible: true
            ParallelAnimation {
                NumberAnimation {
                    properties: "y"
                    duration: 250
                    easing.type: Easing.InOutQuad
                }
            }
        }
    }
}
