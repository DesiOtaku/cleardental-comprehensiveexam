import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.3

Rectangle {
    color: Qt.rgba(0,0,1,.1)

    Label {
        font.pointSize: 16
        text: "Treatment Plan"
        anchors.top: parent.top
        anchors.horizontalCenter: parent.horizontalCenter
    }

}
