#ifndef CDIMAGEMANAGER_H
#define CDIMAGEMANAGER_H

#include <QQuickImageProvider>


class CDImageManager : public QQuickImageProvider
{
public:
    CDImageManager();
    QPixmap requestPixmap(const QString &id, QSize *size, const QSize &requestedSize);
};

#endif // CDIMAGEMANAGER_H
