#ifndef CDTOOLLAUNCHER_H
#define CDTOOLLAUNCHER_H

#include <QObject>

class CDToolLauncher : public QObject
{
    Q_OBJECT
public:
    enum CDTool {
        Invalid,
        MedicalReview,
        Radiograph,
        Extraoral,
        Intraoral,
        TMJ,
        HardTissue
    };
    Q_ENUM(CDTool)
    explicit CDToolLauncher(QObject *parent = nullptr);


signals:

public slots:
    static void launchTool(CDTool whichApp);
};

#endif // CDTOOLLAUNCHER_H
