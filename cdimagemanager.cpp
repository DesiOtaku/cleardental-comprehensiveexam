#include "cdimagemanager.h"

#include "cddefaults.h"

#include <QQuickImageProvider>
#include <QDebug>
#include <QCoreApplication>

CDImageManager::CDImageManager()  : QQuickImageProvider(QQuickImageProvider::Pixmap)
{

}

QPixmap CDImageManager::requestPixmap(const QString &id, QSize *size, const QSize &requestedSize) {
    QPixmap returnMe;

    QString patDir = CDDefaults::getDefaultPatDir();
    QString patName = QCoreApplication::arguments().at(1);
    returnMe.load(patDir+ patName + "/images/" + id);
    size->setWidth(returnMe.width());
    size->setHeight(returnMe.height());
    if(requestedSize.isValid()) {
        returnMe = returnMe.scaled(requestedSize);
    }


    return returnMe;
}
