import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.VirtualKeyboard 2.2
import QtQuick.Layouts 1.3
import dental.clear 1.0

Rectangle {
    color: Qt.darker(rootWin.color,1.2)

    INIReader {
        id: nameReader
        patientName: patientFileName;
        infoType: INIReader.Personal
        groupName: "Name"
    }

    INIReader {
        id: personalReader
        patientName: patientFileName;
        infoType: INIReader.Personal
        groupName: "Personal"
    }

    Column {
        anchors.fill: parent
        anchors.margins: 8
        RowLayout {
            anchors.horizontalCenter: parent.horizontalCenter
            Label {
                font.pointSize: 16
                text:  {
                    if(nameReader.getVarible("PreferredName").length > 0) {
                        return nameReader.getVarible("FirstName") + " \"" +
                                nameReader.getVarible("PreferredName") + "\" " +
                                nameReader.getVarible("LastName");
                    }

                    return nameReader.getVarible("FirstName") + " " +
                            nameReader.getVarible("LastName");
                }
            }
        }

        Image {
            source: "image://cdimage/photograph/extraoral/profile.jpg"
            fillMode: Image.PreserveAspectFit
            height: examButtons.height / 2
            anchors.horizontalCenter: parent.horizontalCenter
        }

        Label {
            text: "DOB: " + personalReader.getVarible("DateOfBirth") + " (18 years old)";
        }
        Label {
            text: "Allergies: None";
        }
        Label {
            text: "Medications: None";
        }
    }

}
