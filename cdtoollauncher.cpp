#include "cdtoollauncher.h"

#include <QProcess>
#include <QCoreApplication>
#include <QDebug>

#define INSTALL_DIR QString("/opt/")

CDToolLauncher::CDToolLauncher(QObject *parent) : QObject(parent)
{

}


void CDToolLauncher::launchTool(CDTool whichApp) {
    QProcess pro;
    QString progString;

    switch (whichApp) {
    case HardTissue:
        progString = "cleardental-reviewofsystems"; //for debugging for now
        break;
    default:
        break;
    }
    pro.setProgram(INSTALL_DIR + progString + "/bin/" + progString);
    QStringList arguments;
    arguments <<QCoreApplication::arguments().at(1);
    pro.setArguments(arguments);
    pro.startDetached();
}
